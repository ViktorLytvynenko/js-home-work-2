/// Theory

// 1. Які існують типи даних у Javascript?
// number, string, boolean, null, undefined, object, symbol

// 2. У чому різниця між == і ===?
// == сравнивает на равенство, а === сравнивает на равенство с учетом проверки типа данных

// 3. Що таке оператор?
// Запуск какого-то действия в коде.



/// Practice

// Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон браузера - alert, prompt, confirm. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// Технічні вимоги:
//
// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? і кнопками Ok, Cancel. Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача. Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).


let name;
let age;
let result;


while (!isNaN(name) || isNaN(age) || age == "") {
    name = prompt("Type please your name", '');
    console.log(name);
    age = Number(prompt("Type please your age", ''));
    console.log(age);
    notAllowed = "You are not allowed to visit this website"
    allowed = "Welcome, " + name
}

    if (age < 18) {
        alert(notAllowed);
    }
    else if (age <=22) {
        console.log(result = confirm("Are you sure you want to continue?"));
        if (result) {
            alert(allowed);
        }
        else {
            alert(notAllowed);
        }
    }
    else {
    alert(allowed)
    }